# Projet Nao - Tic Tac Toe #

## Description ##

Le but du projet est de pouvoir jouer au morpion avec Nao.   
Dans une première version, il faudra donner oralement à Nao les coups joués.  
Il sera capable de jouer ses coups de façon autonome.  
La grille est numéroté de cette manière.
 _______

| 7 | 8 | 9 |

| 4 | 5 | 6 |

| 1 | 2 | 3 |

_______

## Développement ##

Il faut importer le fichier dans le dossier box library.  
Ce fichier contient toutes les box animations et les logiques de jeu.  


## Box ##

### Animation ###

#### Position de démarage ##
Nao s'assoit

#### Position relax ####
Nao lève le bras droit pour se tenir prêt à jouer. Sa main est fermé  
Son bras gauche est sur le côté de manière a laissé le plateau dégagé.

#### Position relax (main ouverte) ####
Nao est en position pour attraper le stylo.

#### Ecrire case 1 ####
Nao se positionne pour écrire en case 1 et dessine une croix

#### Ecrire case 2 ####
Nao se positionne pour écrire en case 2 et dessine une croix.

#### Ecrire case 3 ####
Nao se positionne pour écrire en case 3 et dessine une croix.

#### Ecrire case 4 ####
Nao se positionne pour écrire en case 4 et dessine une croix.

#### Ecrire case 5 ####
Nao se positionne pour écrire en case 5 et dessine une croix.

#### Ecrire case 6 ####
Nao se positionne pour écrire en case 6 et dessine une croix.

#### Ecrire case 7 ####
Nao se positionne pour écrire en case 7 et dessine une croix.

#### Ecrire case 8 ####
Nao se positionne pour écrire en case 8 et dessine une croix.

#### Ecrire case 9 ####
Nao se positionne pour écrire en case 9 et dessine une croix.

### Logique de jeu ###

#### IA Jeu ####
C'est au tour de Nao de jouer il sait quel coups joué. Il n'aime pas perdre.  
Les coups joués sont stockés en mémoire

#### As tu joué ####
Lorsque le joueur a joué son coup il doit le dire à Nao ainsi que la case joué.  
Nao pourra ensuite jouer si il a bien compris ce qu'on lui a dit sinon il demandera de répeter.


# Algo #

## Initialisation ##
Nao prend le stylo

  - Evol: Nao rale et ne commence pas a jouer sans reconnaitre une grille devant lui
  
## Jeu ##

  - Nao demande qui commence
  
### Tour de Nao ###

  Nao écrit à une position donné

### Tour du Joueur ###

  Le joueur écrit sur la grille et dit a nao la coordonnée qu'il a modifié

  - Evol: Nao rale si la case est déjà écrite

   
### Fin de la partie ###

  Nao se moque du joueur si nao gagne,  
  nao dit qu'il s'est laissé gagné si le joueur gagne  
  en cas d'égalité, nao demande de rejouer  

  - Evol: nao s'enerve et gicle l'ardoise
  - Evol: nao reconnait si le jeu n'a pas été effacé