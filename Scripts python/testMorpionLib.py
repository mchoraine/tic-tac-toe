__author__ = 'Goto'

import IA_morpion
import random
def whoGoesFirst():
    # Randomly choose the player who goes first.
    if random.randint(0, 1) == 0:
        return 'computer'
    else:
        return 'player'
def isBoardFull(board):
    # Return True if every space on the board has been taken. Otherwise return False.
    for i in range(1, 10):
        if IA_morpion.isCaseEmpty(board, i):
            return False
    return True

def drawBoard(board):
    # This function prints out the board that it was passed.
    # "board" is a list of 10 strings representing the board (ignore index 0)
    print('   |   |')
    print(' ' + board[7] + ' | ' + board[8] + ' | ' + board[9])
    print('   |   |')
    print('-----------')
    print('   |   |')
    print(' ' + board[4] + ' | ' + board[5] + ' | ' + board[6])
    print('   |   |')
    print('-----------')
    print('   |   |')
    print(' ' + board[1] + ' | ' + board[2] + ' | ' + board[3])
    print('   |   |')

def getPlayerMove(board):
    # Let the player type in his move.
    move = 0
    # while move not in ['1', '2', '3', '4', '5', '6', '7', '8', '9'] or not IA_morpion.isCaseEmpty(board, int(move)):
    while move not in [1, 2, 3, 4, 5, 6, 7, 8, 9] or not IA_morpion.isCaseEmpty(board, move):
        print('What is your next move? (1-9)')
        given = input()
        move = int(given)
    return move


print('Welcome to Tic Tac Toe!')
while True:
    # Reset the board
    theBoard = [" "] * 10
    turn = whoGoesFirst()
    print('The ' + turn + ' will go first.')
    gameIsPlaying = True
    while gameIsPlaying:
        if turn == 'player':
            # Player's turn.
            drawBoard(theBoard)
            move = getPlayerMove(theBoard)
            IA_morpion.fillCase(theBoard, move, "0")
            if IA_morpion.isWinner(theBoard, "0"):
                drawBoard(theBoard)
                print('Hooray! You have won the game!')
                gameIsPlaying = False
            else:
                if isBoardFull(theBoard):
                    drawBoard(theBoard)
                    print('The game is a tie!')
                    break
                else:
                    turn = 'computer'
        else:
            # Computer's turn.
            move = IA_morpion.getNaoMove(theBoard)
            IA_morpion.fillCase(theBoard, move, "1")
            if IA_morpion.isWinner(theBoard, "1"):
                drawBoard(theBoard)

                print('The computer has beaten you! You lose.')
                gameIsPlaying = False
            else:
                if isBoardFull(theBoard):
                    drawBoard(theBoard)
                    print('The game is a tie!')
                    break
                else:
                    turn = 'player'