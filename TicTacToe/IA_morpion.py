__author__ = 'Goto'



def fillCase(board, case, player):
    board[case] = player


def isCaseEmpty(board, case):
    # Retourne true si la case est vide
    return board[case] == ' '


def isWinner(board, player):
    # La fonction retourne true si le joueur a gagne
    # on verifie pour chaque combinaison possible si le joueur a gagne
    return (
        (board[7] == player and board[8] == player and board[9] == player) or # horizontal haut
        (board[4] == player and board[5] == player and board[6] == player) or # horizontal milieu
        (board[1] == player and board[2] == player and board[3] == player) or # horizontal bas
        (board[7] == player and board[4] == player and board[1] == player) or # vertical gauche
        (board[8] == player and board[5] == player and board[2] == player) or # vertical centre
        (board[9] == player and board[6] == player and board[3] == player) or # vertical droite
        (board[7] == player and board[5] == player and board[3] == player) or # diagonale
        (board[9] == player and board[5] == player and board[1] == player)    # diagonale
    )


def getBoardCopy(originalBoard):
    # Copie le tableau
    newBoard = []
    for i in originalBoard:
        newBoard.append(i)
    return newBoard;


# regarde si le joueur va gagner en remplissant la grille de morpion
# on test pour chaque possibilite si ca fait gagner
def canPlayerWinNextMove(board, playerIsNao):
    for i in range(0, 9):
        # on utilise un tableau "virtuel" pour essayer les coups
        boardCopyLocal = getBoardCopy(board)
        # si la case est vide
        if isCaseEmpty(boardCopyLocal, i):
            # on essaye de le remplir
            fillCase(boardCopyLocal, i, playerIsNao)
            # on regarde si le mouvement la f'rai gagner
            if isWinner(boardCopyLocal, playerIsNao):
                return i



def getNextMove(board):
    #on trie nos mouvements possible par impact d'abord diagonale, puis centre puis
    orderedMoves = [1, 7, 9, 3, 5, 2, 4 ,6, 8]
    for i in orderedMoves:
        if isCaseEmpty(board, i):
            return i



def getNaoMove(board):
    # Algorithme de l'algo du morpion
    # Est ce qu'on peut gagner au prochain tour
    caseToMove = canPlayerWinNextMove(board, "nao")
    if(caseToMove is not None):
        return caseToMove
    # Est ce que l'adversaire peut gagner au prochain tour
    caseToMove = canPlayerWinNextMove(board, "human")
    if(caseToMove is not None):
        return caseToMove
    # Try to take one of the corners, if they are free.
    caseToMove = getNextMove(board)
    if(caseToMove is None):
        return 99
    return caseToMove



