<?xml version="1.0" encoding="UTF-8" ?>
<Package name="TicTacToe" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="Jeu" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="ExampleDialog" src="Jeu/ExampleDialog/ExampleDialog.dlg" />
        <Dialog name="dialogHumanPlay" src="dialogHumanPlay/dialogHumanPlay.dlg" />
        <Dialog name="Confirmation" src="Confirmation/Confirmation.dlg" />
        <Dialog name="WhoBegin" src="WhoBegin/WhoBegin.dlg" />
        <Dialog name="YesNo" src="YesNo/YesNo.dlg" />
    </Dialogs>
    <Resources>
        <File name="Box library" src="Box library.cbl" />
        <File name="IA_morpion" src="IA_morpion.py" />
        <File name="choice_sentences" src="Jeu/Aldebaran/choice_sentences.xml" />
    </Resources>
    <Topics>
        <Topic name="ExampleDialog_frf" src="Jeu/ExampleDialog/ExampleDialog_frf.top" topicName="ExampleDialog" language="fr_FR" />
        <Topic name="dialogHumanPlay_frf" src="dialogHumanPlay/dialogHumanPlay_frf.top" topicName="dialogHumanPlay" language="fr_FR" />
        <Topic name="Confirmation_frf" src="Confirmation/Confirmation_frf.top" topicName="Confirmation" language="fr_FR" />
        <Topic name="WhoBegin_frf" src="WhoBegin/WhoBegin_frf.top" topicName="WhoBegin" language="fr_FR" />
        <Topic name="YesNo_frf" src="YesNo/YesNo_frf.top" topicName="YesNo" language="fr_FR" />
    </Topics>
    <IgnoredPaths />
</Package>
